from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]
    # BELOW CODE: this is additional stuff that Abraham found to make it so we can't
    # see another specific users account and category info when signed in as someone else
    # def __init__(self, user, *args, **kwargs):
    #     super(ReceiptForm, self).__init__(*args, **kwargs)
    #     self.fields["category"].queryset = ExpenseCategory.objects.filter(
    #         owner=user
    #     )
    #     self.fields["account"].queryset = Account.objects.filter(owner=user)
    # # END OF ADDITIONAL CODE for Feature 15 Personal Tests Resolution


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
